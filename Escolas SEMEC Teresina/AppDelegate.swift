//
//  AppDelegate.swift
//  Escolas SEMEC Teresina
//
//  Created by CTI on 20/02/17.
//  Copyright (c) 2017 PosMobile. All rights reserved.
//

import UIKit
import RealmSwift
import FacebookCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //Configuracao do Realm
        let config = Realm.Configuration(            
            schemaVersion: 1,
            deleteRealmIfMigrationNeeded: true
        )
        
        Realm.Configuration.defaultConfiguration = config
        
        let email = UserDefaults.standard.string(forKey: "email")
        if email != nil && !(email?.isEmpty)! {
            
            let realm = try! Realm()
            let predicate = NSPredicate(format: "email = %@", email!)
            let us = realm.objects(Usuario.self).filter(predicate)
            
            if us.count > 0 {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc: ListagemController = storyboard.instantiateViewController(withIdentifier: "ListaVC") as! ListagemController
                vc.alertaUsuario = true
                vc.nomeUsuario = (us.first?.nome)!
            
                if let window = self.window {
                    let navigationController = UINavigationController(rootViewController: vc)
                    window.rootViewController = navigationController
                }
                
            }else{
                UserDefaults.standard.set(nil, forKey: "email")
            }
            
        }
        
        
        return SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool
    {
        return SDKApplicationDelegate.shared.application(app, open: url,    options: options)
    }
    


}

