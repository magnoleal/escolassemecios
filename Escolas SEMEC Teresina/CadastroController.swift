//
//  CadastroController.swift
//  Escolas SEMEC Teresina
//
//  Created by CTI on 20/02/17.
//  Copyright (c) 2017 PosMobile. All rights reserved.
//

import UIKit
import RealmSwift

class CadastroController: UIViewController {

    @IBOutlet weak var nome: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var senha: UITextField!
    @IBOutlet weak var confirmacao: UITextField!
    
    @IBAction func cadastrar() {        
        
        if nome.text!.isEmpty ||
           email.text!.isEmpty ||
           senha.text!.isEmpty ||
           confirmacao.text!.isEmpty ||
           senha.text! != confirmacao.text!{
                
            let alert = UIAlertController(title: "Atenção", message: "Preencha todos os campos corretamente", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
                
            return
        }
        
        let realm = try! Realm()
        let predicate = NSPredicate(format: "email = %@", email.text!)
        let us = realm.objects(Usuario.self).filter(predicate)
        
        if us.count > 0{
            let alert = UIAlertController(title: "Atenção", message: "Email já cadastrado anterioremente", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let usuario = Usuario()
        usuario.nome = nome.text!
        usuario.email = email.text!
        usuario.senha = senha.text!
        usuario.facebook = false
        
        try! realm.write {
            realm.add(usuario)
        }
        
        UserDefaults.standard.set(email.text!, forKey:"email")
        UserDefaults.standard.synchronize()
        
        let vc: ListagemController = self.storyboard?.instantiateViewController(withIdentifier: "ListaVC") as! ListagemController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    

}
