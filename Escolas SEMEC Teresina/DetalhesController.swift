//
//  DetalhesController.swift
//  Escolas SEMEC Teresina
//
//  Created by CTI on 20/02/17.
//  Copyright (c) 2017 PosMobile. All rights reserved.
//

import UIKit
import MapKit

class DetalhesController: UIViewController {

    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var telefone: UILabel!
    @IBOutlet weak var alunos: UILabel!
    @IBOutlet weak var salas: UILabel!
    @IBOutlet weak var turmas: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var escola: Escola?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Detalhes da Escola"

        self.nome.text = escola?.nome
        self.telefone.text = escola?.telefone
        self.alunos.text = escola?.numAlunos?.description
        self.turmas.text = escola?.numTurmas?.description
        self.salas.text = escola?.numSalas?.description
        
        let latitude:CLLocationDegrees = (escola?.latitude)!
        let longitude:CLLocationDegrees = (escola?.longitude)!
        
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = escola?.nome
        annotation.subtitle = "Localização da Escola"
        mapView.addAnnotation(annotation)
        
        
    }

}
