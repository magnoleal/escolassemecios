//
//  Escola.swift
//  Escolas SEMEC Teresina
//
//  Created by CTI on 20/02/17.
//  Copyright (c) 2017 PosMobile. All rights reserved.
//

import UIKit

class Escola {
    
    var id: String?
    var nome: String?
    var latitude: Double?
    var longitude: Double?
    var telefone: String?
    var numAlunos: Int?
    var numTurmas: Int?
    var numSalas: Int?
    
    init(){
        
    }
    
    init(id: String, nome: String, latitude: Double, longitude: Double, telefone: String,
        numAlunos: Int, numTurmas: Int, numSalas: Int){
        self.id = id
        self.nome = nome
        self.latitude = latitude
        self.longitude = longitude
        self.telefone = telefone
        self.numAlunos = numAlunos
        self.numTurmas = numTurmas
        self.numSalas = numSalas
    }
    
    init(dictionary: [String:Any]) {
        self.id = dictionary["CodSemec"] as? String
        self.nome = dictionary["nome"] as? String
        self.latitude = Double(dictionary["latitude"] as! String)
        self.longitude = Double(dictionary["longitude"] as! String)
        self.telefone = dictionary["Telefones"] as? String
        self.numAlunos = Int(dictionary["aluno"] as! String)
        self.numTurmas = Int(dictionary["turmas"] as! String)
        self.numSalas = Int(dictionary["salas"] as! String)
    }
    
}
