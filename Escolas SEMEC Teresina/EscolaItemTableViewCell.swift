//
//  EscolaItemTableViewCell.swift
//  Escolas SEMEC Teresina
//
//  Created by CTI on 21/02/17.
//  Copyright © 2017 PosMobile. All rights reserved.
//

import UIKit

class EscolaItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var escolaTelefone: UILabel!
    @IBOutlet weak var escolaNome: UILabel!
    
    func setEscola(_ escola: Escola) {
        self.escolaNome.text = escola.nome
        self.escolaTelefone.text = "Telefone: "+escola.telefone!
    }
    
}
