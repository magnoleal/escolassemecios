//
//  EscolaPointAnnotation.swift
//  Escolas SEMEC Teresina
//
//  Created by CTI on 22/02/17.
//  Copyright © 2017 PosMobile. All rights reserved.
//

import UIKit
import MapKit

class EscolaPointAnnotation: MKPointAnnotation {
    var escola: Escola
    init(escola: Escola) {
        self.escola = escola
    }
}
