//
//  ListagemController.swift
//  Escolas SEMEC Teresina
//
//  Created by CTI on 20/02/17.
//  Copyright (c) 2017 PosMobile. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD

class ListagemController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var alertaUsuario: Bool = false
    var nomeUsuario: String = ""
    var escolas : [Escola]?
    @IBOutlet weak var tableView: UITableView!
    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        
         //soh para mostrar o usuario que esta logado
        if(alertaUsuario){
                                    
            let alert = UIAlertController(title: "Bem vindo "+nomeUsuario, message: "Deseja sair entrar com outro usuário?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Não", style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction(title: "Sim", style: UIAlertActionStyle.destructive, handler: { action in
                
                UserDefaults.standard.set(nil, forKey: "email")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc : LoginController = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginController
                                
                if let window = self.window {
                    let navigationController = UINavigationController(rootViewController: vc)
                    window.rootViewController = navigationController
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            }))            
            
            self.present(alert, animated: true, completion: nil)
        }
        
        reloadLista()
    }
    
    /**
    * Pegando os dados do webservice
    **/
    private func reloadLista(){
        
        self.escolas = [Escola]();
        
        HUD.show(.progress)
        Alamofire.request("http://www.semec.pi.gov.br/webservice_semec/webservicesemec_teste/escolas").responseJSON { response in
            //debugPrint(response)
            
            HUD.hide()
            
            switch response.result {
            case .success:
                if let jsonDict = response.result.value as? [String:Any],
                    let dataArray = jsonDict["dados"] as? [[String:Any]] {
                    
                    for data in dataArray {
                        let escola = Escola(dictionary: data)
                        self.escolas?.append(escola)
                    }
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
        
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if self.escolas != nil {
            return self.escolas!.count
        }else{
            return 0
        }
        
    }
    
    /**
     * Montando a view de cada registro
     **/
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : EscolaItemTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EscolaItemTableViewCellId", for: indexPath) as!EscolaItemTableViewCell        
        let escola = self.escolas?[indexPath.row]
        cell.setEscola(escola!)        
        return cell
    }
    
    /**
     * Acao do clique em cada item
     **/
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let escola = self.escolas?[indexPath.row]
        let detailsView: DetalhesController = self.storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetalhesController
        detailsView.escola = escola
        self.navigationController?.pushViewController(detailsView, animated: true)
    }
    
}
