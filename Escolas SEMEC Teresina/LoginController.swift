//
//  LoginController.swift
//  Escolas SEMEC Teresina
//
//  Created by CTI on 20/02/17.
//  Copyright (c) 2017 PosMobile. All rights reserved.
//

import UIKit
import RealmSwift
import FacebookLogin
import FacebookCore
import PKHUD

class LoginController: UIViewController {

    //Campos da View
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var senha: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //escondendo o botao voltar
        self.navigationItem.hidesBackButton = true
    }

    /**
    * Acao da entrar via login e senha
    */
    @IBAction func entrar() {
        print(email.text!)
        print(senha.text!)
        
        
        let realm = try! Realm()
        let predicate = NSPredicate(format: "facebook = false AND email = %@ AND senha = %@", email.text!, senha.text!)
        let us = realm.objects(Usuario.self).filter(predicate)
        
        if us.count <= 0{
            let alert = UIAlertController(title: "Atenção", message: "Usuário ou senha inválidos", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        UserDefaults.standard.set(email.text!, forKey:"email")
        UserDefaults.standard.synchronize()
        
        let vc: ListagemController = self.storyboard?.instantiateViewController(withIdentifier: "ListaVC") as! ListagemController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    /**
     * Acao da entrar via Facebook
     */
    @IBAction func entrarFacebook() {
        
        let loginManager = LoginManager()
        
        //chamando modal login do Facebook
        
        loginManager.logIn([ .publicProfile, .email ], viewController: self) { loginResult in
            HUD.show(.progress)
            switch loginResult {
            case .failed(let error): //caso der erro
                print(error)
                HUD.hide()
                let alert = UIAlertController(title: "Atenção", message: "Erro ao entrar via Facebook", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            case .cancelled: //caso o usuario cancele
                print("User cancelled login.")
                HUD.hide()
                break
            case .success( _, _, _): //caso der cetro
                let connection = GraphRequestConnection()
                
                //pegando dados do usuario
                
                connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"])) { httpResponse, result in
                    switch result {
                    case .success(let response):
                        
                        let realm = try! Realm()
                        let predicate = NSPredicate(format: "email = %@", response.dictionaryValue?["email"] as! String)
                        let us = realm.objects(Usuario.self).filter(predicate)
                        
                        var emailUser:String = ""
                        
                        if us.count <= 0 { //se nao existe cria um usuario
                            let usuario = Usuario()
                            usuario.nome = response.dictionaryValue?["name"] as! String
                            usuario.email = response.dictionaryValue?["email"] as! String
                            usuario.senha = ""
                            usuario.facebook = true
                            
                            try! realm.write {
                                realm.add(usuario)
                            }
                            
                            emailUser = usuario.email
                            
                            
                        }else{ //se nao existe pega o usuario
                            emailUser = (us.first?.nome)!
                        }
                        
                        HUD.hide()
                        
                        UserDefaults.standard.set(emailUser, forKey:"email")
                        UserDefaults.standard.synchronize()
                        
                        let vc: ListagemController = self.storyboard?.instantiateViewController(withIdentifier: "ListaVC") as! ListagemController
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    case .failed(let _):
                        HUD.hide()
                        let alert = UIAlertController(title: "Atenção", message: "Erro ao acessar seus dados do Facebook", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                connection.start()
                break
            }
        }
    }

}
