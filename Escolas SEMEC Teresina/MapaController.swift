//
//  MapaController.swift
//  Escolas SEMEC Teresina
//
//  Created by CTI on 20/02/17.
//  Copyright (c) 2017 PosMobile. All rights reserved.
//

import UIKit
import MapKit
import PKHUD
import Alamofire

class MapaController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    //var escolas : [Escola]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadLista()
    }

    /**
     * Pegando os dados do webservice
     **/
    private func reloadLista(){
        
        
        HUD.show(.progress)
        Alamofire.request("http://www.semec.pi.gov.br/webservice_semec/webservicesemec_teste/escolas").responseJSON { response in
            //debugPrint(response)
            
            HUD.hide()
            
            switch response.result {
            case .success:
                if let jsonDict = response.result.value as? [String:Any],
                    let dataArray = jsonDict["dados"] as? [[String:Any]] {
                    
                    var escola:Escola? = nil;
                    
                    for data in dataArray {
                        escola = Escola(dictionary: data)
                        
                        let latitude:CLLocationDegrees = (escola?.latitude)!
                        let longitude:CLLocationDegrees = (escola?.longitude)!
                        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
                        
                        //Annotation personalizada para armazenar a escola de cada marcacao
                        let annotation = EscolaPointAnnotation(escola: escola!)
                        annotation.coordinate = location
                        annotation.title = escola?.nome
                        self.mapView.addAnnotation(annotation)
                        
                    }
                    
                    //zoom
                    if escola != nil{
                        let latitude:CLLocationDegrees = -5.096987
                        let longitude:CLLocationDegrees = -42.769852
                        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
                        let span = MKCoordinateSpanMake(0.35, 0.35)
                        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
                        self.mapView.setRegion(region, animated: true)
                    }
                    
                }
            case .failure(let error):
                print(error)
            }
        }
        
        
    }
    
    /**
     * Acao de clique no balao de cada marcador
     **/
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView){
        
        guard view.annotation is EscolaPointAnnotation else {
            return
        }
        
        let escolaAnnotation = view.annotation as! EscolaPointAnnotation
        
        let detailsView: DetalhesController = self.storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetalhesController
        detailsView.escola = escolaAnnotation.escola
        self.navigationController?.pushViewController(detailsView, animated: true)
    }

}
