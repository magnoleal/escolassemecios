//
//  Usuario.swift
//  Escolas SEMEC Teresina
//
//  Created by CTI on 20/02/17.
//  Copyright (c) 2017 PosMobile. All rights reserved.
//

import UIKit
import RealmSwift

class Usuario: Object {
    
    dynamic var nome: String = ""
    dynamic var email: String = ""
    dynamic var senha: String = ""
    dynamic var facebook: Bool = false
    
    override static func primaryKey() -> String? {
        return "email"
    }        
    
}
