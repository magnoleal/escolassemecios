#Trabalho final da disciplina de iOS#

**Especialização em Desenvolvimento de Aplicações para Dispositivos Móveis - FATEPI**

**Professor: Wesley Galindo**

**Grupo:**

* Ênio Rodrigues <eniocc@gmail.com>
* Hildebrando Segundo <hildebrandosegundo@gmail.com>
* José Magno <magnoleal89@gmail.com>

### Projeto: ###
O projeto apresenta uma visualização das escolas municipais do município de Teresina - PI.
Contendo informações como, sua localização, quantidade de alunos, turmas e etc.

### Bibliotecas Utilizadas: ###
* PKHUD: Loading progress
* Alamofire: Http request
* RealmSwift: Database
* Facebook: Login alternativo